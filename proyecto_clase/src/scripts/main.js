(function() {
	var loadCSS = require('./lib/loadCSS');
	var onScroll = require('./lib/onScroll');

	document.addEventListener('DOMContentLoaded', onDOMLoad);

	function onDOMLoad(){
		var btnMenu = document.getElementById('btnMenu');
		var navbarMenu = document.getElementById('navbarMenu');

		btnMenu.addEventListener('click', onClickMenu);
		window.addEventListener('scroll', onScroll);

		loadCSS('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css');
		loadCSS('https://fonts.googleapis.com/css?family=Montserrat|Lato');
		
		function onClickMenu() {
			navbarMenu.classList.toggle('header-menu-list--show')
		}
	}
}());