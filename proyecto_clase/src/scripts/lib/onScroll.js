var onScroll = function(){
	var firstItem = document.querySelector('.content-block');
	var headerElem = document.querySelector('.header');	
	var breakpoint = firstItem.getBoundingClientRect().top + 120;

	if(window.scrollY > breakpoint){
		headerElem.classList.add('header--light');
	}else{
		headerElem.classList.remove('header--light');
	}
}

module.exports = onScroll;